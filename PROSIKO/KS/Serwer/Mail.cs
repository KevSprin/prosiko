﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSK
{

    class MessagePair
    {
        string From;
        string Message;
        public MessagePair(string From, string Message)
        {
            this.From = From;
            this.Message = Message;
        }

        public string GetFrom()
        {
            return From;
        }

        public string GetMessage()
        {
            return Message;
        }
    }

    public class Mail : ICommandAnswerer
    {
        Dictionary<string, MessagePair> MailMessages = new Dictionary<string, MessagePair>();

        public string AnswerCommand(string command)
        {
            string return_message = "";
            string user = command.Split()[2];
            switch (command.Split()[1])
            {
                case "get":
                    return_message = GetMessage(user);
                    break;
                case "send":
                    string sendTo = command.Split()[3];
                    string message = command.Substring(command.IndexOf(sendTo) + sendTo.Length + 1);
                    return_message = SendMessage(user, sendTo, message); 
                    break;
            }
            return return_message;
        }

        private string SendMessage(string From, string To, string Message)
        {
            MessagePair messagePair = new MessagePair(From, Message);
            string message;
            try
            {
                MailMessages.Add(To, messagePair);
                message = $"Message to {To} has been sent!";
            }
            catch
            {
                message = $"Message could NOT be delivered! {To} haven't read his mail yet!";
            }         
            return message;
        }

        private string GetMessage(string User)
        {
            MessagePair messagePair;
            string message;
            try
            {
                messagePair = MailMessages[User];
                message = $"From : {messagePair.GetFrom()}\nMessage: {messagePair.GetMessage()}";
                MailMessages.Remove(User);
            }
            catch
            {
                message = "Your mailbox is empty!";
            }
            return message;
        }

    }
}
