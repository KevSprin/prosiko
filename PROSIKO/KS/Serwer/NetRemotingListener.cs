﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;

namespace PSK
{
    public class NetRemotingListener : IListener
    {
        private NetRemotingCommunicator remotableObject;
        public void Start(CommunicatorD onConnect)
        {
            remotableObject = new NetRemotingCommunicator();
            TcpChannel channel = new TcpChannel(8080);
            ChannelServices.RegisterChannel(channel, false);
            RemotingServices.Marshal(remotableObject, "Test"); // tworzy singletona tak jak należy
            //RemotingConfiguration.RegisterWellKnownServiceType(typeof(NetRemotingCommunicator), "Test", WellKnownObjectMode.Singleton); <- nie działa jak należy
            
            onConnect(remotableObject);
            ChannelServices.UnregisterChannel(channel);
            RemotingServices.Disconnect(remotableObject);
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }
    }
}
