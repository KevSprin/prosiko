﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PSK
{
    public class TCPListener : IListener
    {
        private TcpListener server;
        private CommunicatorD onConnect;
        public TCPListener()
        {
            server = new TcpListener(IPAddress.Any, 12345);
            server.Start();
        }
        public void Start(CommunicatorD onConnect)
        {
            this.onConnect = onConnect;

            while (true)
            {
                // dodajemy Klienta żądającego połączenia
                TcpClient client = server.AcceptTcpClient();
                // umożliwianie połączenie dla wielu klientów
                Thread t = new Thread(new ParameterizedThreadStart(HandleCommunicator));
                t.Start(client);
            }
        }

        public void HandleCommunicator(object client)
        {
            var communicator = new TCPCommunicator((TcpClient)client);
            onConnect(communicator);
        }

        public void Stop()
        {
            server.Stop();
        }
    }
}
