﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PSK
{
    public class UDPCommunicator : ICommunicator
    {
        private UdpClient client;
        private int port;
        private int max_porcja = 1500;
        private CommunicatorD onDisconnect;

        public string Name => "udp";

        public UDPCommunicator(int port)
        {
            client = new UdpClient(port);
            this.port = port;
            client.DontFragment = false;
        }

        public void Start(CommandD onCommand, CommunicatorD onDisconnect)
        {
            this.onDisconnect = onDisconnect;
            while (true)
                try
                {
                    IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
                    var receiveBytes = client.Receive(ref RemoteIpEndPoint);
                    string returnData = Encoding.ASCII.GetString(receiveBytes);
                    while (receiveBytes.Length == max_porcja)
                    {
                        receiveBytes = client.Receive(ref RemoteIpEndPoint);
                        returnData += Encoding.ASCII.GetString(receiveBytes);
                    }

                    Console.WriteLine($"Otrzymałem wiadomość: {returnData.ToString()} , przez UDP");
                    Console.WriteLine($"Wysłana przez protokół UDP na adresie {RemoteIpEndPoint.Address.ToString()} na porcie {RemoteIpEndPoint.Port.ToString()}");
                    byte[] sendBytes = Encoding.ASCII.GetBytes(onCommand(returnData));

                    Console.WriteLine($"Zwracam wiadomość: {Encoding.UTF8.GetString(sendBytes)} , przez UDP");
                    if(sendBytes.Length > max_porcja)
                    {
                        var porcje = PorcjonowanieDanych(sendBytes);
                        foreach (var porcja in porcje)
                        {
                            client.Send(porcja, porcja.Length, RemoteIpEndPoint);
                            Thread.Sleep(50); // jest po to aby klient mógł spokojnie odbierać bajty
                        }
                    }
                    else
                        client.Send(sendBytes, sendBytes.Length, RemoteIpEndPoint);
                }
                catch (ObjectDisposedException)
                {
                    break;
                }
                catch (SocketException)
                {
                    Console.WriteLine("Połaczenie UDP zamknięte");
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    break;
                }
        }

        private IList<byte[]> PorcjonowanieDanych(byte[] dane)
        {
            IList<byte[]> porcje = new List<byte[]>();
            int start = 0;
            int mnoznik = dane.Length / max_porcja;
            int reszta = dane.Length - max_porcja * mnoznik;
            
            for(int i = 0; i < mnoznik; ++i)
            {
                byte[] tmp = new byte[max_porcja];
                Array.Copy(dane, start, tmp, 0, max_porcja);
                porcje.Add(tmp);
                start += max_porcja;
            }
            byte[] reszta_danych = new byte[reszta];
            Array.Copy(dane, start, reszta_danych, 0, reszta);
            porcje.Add(reszta_danych);
            return porcje;
        }

        public void Stop()
        {
            client.Close();
            onDisconnect(this);
        }
    }
}
