﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSK
{
    public class FileCommunicator : ICommunicator
    {
        private string file_name;
        private string directory = null;
        private string full_path = null;
        private FileSystemWatcher fileSystemWatcher = new FileSystemWatcher();
        private string message = null;
        public string Name => "file";
        private CommunicatorD onDisconnect;

        public FileCommunicator(string directory)
        {
            this.directory = directory;
            fileSystemWatcher.Path = directory;
            fileSystemWatcher.Changed += FileSystemWatcher_Changed;
            fileSystemWatcher.Created += FileSystemWatcher_Created;
            fileSystemWatcher.EnableRaisingEvents = true;
            fileSystemWatcher.Filter = "*.in";
        }

        public static bool IsFileReady(string filename)
        {
            // If the file can be opened for exclusive access it means that the file
            // is no longer locked by another process.
            try
            {
                using (FileStream inputStream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.None))
                    return inputStream.Length > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void FileSystemWatcher_Changed(object sender, FileSystemEventArgs e)
        {

            while (true)
            {
                if (!IsFileReady(e.FullPath)) continue;
                using (StreamReader sr = new StreamReader(e.FullPath))
                {
                    message = sr.ReadToEnd();
                }
                break;
            }
            File.Delete(e.FullPath);
        }

        private void FileSystemWatcher_Created(object sender, FileSystemEventArgs e)
        {
            if (Path.GetExtension(e.FullPath) == ".in")
            {
                file_name = Path.GetFileNameWithoutExtension(e.FullPath) + ".out";
                full_path = Path.GetDirectoryName(e.FullPath) + $@"\{file_name}";
            }
        }
        public void Start(CommandD onCommand, CommunicatorD onDisconnect)
        {
            this.onDisconnect = onDisconnect;
            while (true)
            {
                if(message != null)
                {
                    try
                    {
                        string answer = onCommand(message);
                        Console.Write("Zwracam: " + answer);

                        File.WriteAllText(full_path, answer);
                        message = null;
                    }
                    catch (TimeoutException)
                    {
                        File.Delete(full_path);
                        return;
                    }
                    catch (Exception)
                    {
                        break;
                    }
                }
            }
        }

        public void Stop()
        {
            onDisconnect(this);
        }
    }
}
