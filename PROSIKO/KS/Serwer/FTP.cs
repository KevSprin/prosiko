﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSK
{
    public class FTP : ICommandAnswerer
    {
        private string SciezkaPlikowSerwera = Directory.GetCurrentDirectory() + @"\..\..\PlikiSerwer";
        public string AnswerCommand(string command)
        {
            string return_message = "";
            string mode = command.Split()[1];
            string sciezka = $@"{SciezkaPlikowSerwera}\{command.Split()[2]}";
            switch (mode)
            {
                case "send":
                    string plik_base64 = command.Split()[3];
                    byte[] dane_pliku = Convert.FromBase64String(plik_base64);
                    using (var fs = new FileStream(sciezka, FileMode.Create, FileAccess.Write))
                    {
                        fs.Write(dane_pliku, 0, dane_pliku.Length);
                    }
                    return_message = "Plik zostal pomyslnie wyslany do serwera.";
                    break;
                case "get":
                    dane_pliku = File.ReadAllBytes(sciezka);
                    plik_base64 = Convert.ToBase64String(dane_pliku);
                    return_message = plik_base64;
                    break;
            }
            return return_message;
        }
    }
}
