﻿namespace PSK
{
    public interface IListener
    {
        void Start(CommunicatorD onConnect);
        void Stop();
    }
}