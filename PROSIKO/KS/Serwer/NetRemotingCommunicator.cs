﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PSK
{
    public class NetRemotingCommunicator : MarshalByRefObject, ICommunicator
    {
        private CommandD onCommand;
        private CommunicatorD onDisconnect;
        private bool IsConnected = true;
        public string Name => ".net";

        public void Start(CommandD onCommand, CommunicatorD onDisconnect)
        {
            this.onCommand = onCommand;
            this.onDisconnect = onDisconnect;
            while (IsConnected)
            {
            }
        }

        public string GetCall(string message)
        {
            Console.WriteLine($"Otrzymałem wiadomość: {message} , przez .net Remoting");
            var odpowiedz = onCommand(message);
            Console.WriteLine($"Zwracam wiadomość: {odpowiedz} , przez .net Remoting");
            return odpowiedz;
        }

        public override object InitializeLifetimeService() { return null; } // ma zapobiec nagłego zgarbage-collectowania

        public void Stop()
        {
            IsConnected = false;
            onDisconnect(this);
        }
    }
}
