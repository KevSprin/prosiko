﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace PSK
{
    public class RS232Communicator : ICommunicator
    {
        private SerialPort client;
        private string message = null;
        public string Name => "rs";
        private CommunicatorD onDisconnect;

        public RS232Communicator(string port, int baud)
        {
            client = new SerialPort();
            client.PortName = port;
            client.BaudRate = baud;
            client.Parity = Parity.None;
            client.DataBits = 8;
            client.StopBits = StopBits.One;
            client.Handshake = Handshake.None;
            client.WriteTimeout = 500;
            client.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
        }

        public void Start(CommandD onCommand, CommunicatorD onDisconnect)
        {
            this.onDisconnect = onDisconnect;
            //if (!client.IsOpen)
            client.Open();
            client.Write("Open");
            while (true)
            {
                if (message != null)
                {
                    try
                    {
                        string answer = onCommand(message);
                        answer = answer.Remove(answer.Length - 1);
                        Console.WriteLine($"Zwracam wiadomość: {answer} , przez RS-232");
                        client.Write(answer);
                        message = null;
                    }
                    catch (InvalidOperationException)
                    {
                        Console.WriteLine("Polaczenie RS-232 zamkniete");
                        break;
                    }
                    catch (TimeoutException)
                    {
                        client.Close();
                        return;
                    }
                }
            }
        }

        public void Stop()
        {
            client.Write("Closed");
            client.Close();
            onDisconnect(this);
        }

        private void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            message = sp.ReadExisting();
            Console.WriteLine($"Orzymałem wiadomość: {message} , przez RS-232");
        }
    }
}
