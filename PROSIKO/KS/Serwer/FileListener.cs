﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSK
{
    public class FileListener : IListener
    {
        private string path = Directory.GetCurrentDirectory() + @"\..\..\..\Pliki";
        private FileCommunicator fileCommunicator;
        public void Start(CommunicatorD onConnect)
        {
            while (true)
            {
                fileCommunicator = new FileCommunicator(path);
                onConnect(fileCommunicator);
            }
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }
    }
}
