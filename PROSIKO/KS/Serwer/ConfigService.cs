﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PSK
{
    
    internal class ConfigService : ICommandAnswerer
    {
        private Dictionary<string, ICommandAnswerer> services;
        private List<ICommunicator> communicators;
        private List<string> media;
        private Action<IListener, string> AddListener;
        private Action<string, ICommandAnswerer> AddService;

        public ConfigService(ref Dictionary<string, ICommandAnswerer> services, ref List<ICommunicator> communicators, ref List<string> media, Action<IListener, string> AddListener, Action<string, ICommandAnswerer> AddService)
        {
            this.services = services;
            this.communicators = communicators;
            this.media = media;
            this.AddListener = AddListener;
            this.AddService = AddService;

        }
        public string AnswerCommand(string command)
        {
            // switch na możliwe polecenia   
            
            string option = command.Split()[1];
            switch (option)
            {
                case "media":
                    string result = "Dostepne media: ";
                    foreach (var item in media)
                    {
                        result += item + "; ";
                    }
                    return result;
                case "uslugi":
                    result = "Dostepne uslugi: ";
                    foreach (var item in services)
                    {
                        result += item.Key + "; ";
                    }
                    return result;
                case "start":
                    string medium = command.Split()[2];
                    var communicator = communicators.Find(x => x.Name == medium);
                    if(communicator == null)
                    {
                        switch (medium)
                        {
                            case "tcp":
                                TCPListener tcp_listener = new TCPListener();
                                AddListener(tcp_listener, "tcp");
                                return "Medium TCP uruchomione";
                            case "udp":
                                UDPListener udp_listener = new UDPListener();
                                AddListener(udp_listener, "udp");
                                return "Medium UDP uruchomione";
                            case "file":
                                FileListener file_listener = new FileListener();
                                AddListener(file_listener, "file");
                                return "Medium File uruchomione";
                            case ".net":
                                NetRemotingListener netRemotingListener = new NetRemotingListener();
                                AddListener(netRemotingListener, ".net");
                                return "Medium .net Remoting uruchomione";
                            case "rs":
                                RS232Listener rS232Listener = new RS232Listener();
                                AddListener(rS232Listener, "rs");
                                return "Medium RS-232 uruchomione";
                        }
                    }
                    return "Dane Medium jest już uruchomione!";
                case "stop":
                    medium = command.Split()[2];
                    communicator = communicators.Where(x => x.Name == medium).First();
                    communicator.Stop();
                    media.Remove(medium);
                    result = $"Medium {medium} zostalo wylaczone";
                    return result;
                case "stop_usluga":
                    string parametr = command.Split()[2];
                    if ("cfg" == parametr) return "Nie można wyłączyć cfg!";
                    var usluga = services.Where(x => x.Key == parametr).First();
                    services.Remove(usluga.Key);
                    result = $"Usluga {usluga.Key} zostala wylaczona";
                    return result;
                case "start_usluga":
                    parametr = command.Split()[2];
                    switch (parametr)
                    {
                        case "ping":
                            PingPong pp = new PingPong();
                            AddService("ping", pp);
                            return "Usluga Ping zostala uruchomiona";
                        case "ftp":
                            FTP ftp = new FTP();
                            AddService("ftp", ftp);
                            return "Usluga FTP zostala uruchomiona";
                        case "mail":
                            Mail mail = new Mail();
                            AddService("mail", mail);
                            return "Usluga Mail zostala uruchomiona";
                    }
                    return "Dana Usluga jest juz uruchomiona!";
                default:
                    break;
            }
            return "";
        }
    }
}