﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace PSK
{
    public class RS232Listener : IListener
    {
        private RS232Communicator communicator;
        public void Start(CommunicatorD onConnect)
        {
            communicator = new RS232Communicator("COM3", 57600);
            onConnect(communicator);
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }
    }
}
