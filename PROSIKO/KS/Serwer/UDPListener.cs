﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSK
{
    public class UDPListener : IListener
    {
        private UDPCommunicator communicator;
        public void Start(CommunicatorD onConnect)
        {           
            communicator = new UDPCommunicator(12346);
            onConnect(communicator);
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }
    }
}
