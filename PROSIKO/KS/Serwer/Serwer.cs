﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PSK
{
    class Serwer
    {
        List<IListener> listeners = new List<IListener>();
        List<ICommunicator> communicators = new List<ICommunicator>();
        Dictionary<string, ICommandAnswerer> services = new Dictionary<string, ICommandAnswerer>();
        List<string> media = new List<string>();

        void AddListener(IListener listener, string medium)
        {
            // uruchamiamy server oraz dodajemy funkcje AddCommunicator żeby dodać Communicator do zbioru communicators
            listeners.Add(listener);
            Thread t = new Thread(new ThreadStart(delegate
            {
                listener.Start(AddCommunicator);
                
            }));
            t.Start();
            media.Add(medium);

        }

        void AddCommunicator(ICommunicator communicator)
        {
            // dodajemy Communicator do zbioru communicators oraz uruchamiamy Communicator przekazując funkcje ServiceCenter oraz CommunicatorDisconnect
            communicators.Add(communicator);
            communicator.Start(ServiceCenter, CommunicatorDisconnect);
        }

        private void CommunicatorDisconnect(ICommunicator commander)
        {
            communicators.Remove(commander);
        }

        void AddService(string name, ICommandAnswerer service)
        {
            services.Add(name, service);
        }

        Serwer()
        {
            // usługa konfiguracji
            ConfigService cs = new ConfigService(ref services, ref communicators, ref media, AddListener, AddService);
            AddService("cfg", cs);
            PingPong pp = new PingPong();
            AddService("ping", pp);
            Mail mail = new Mail();
            AddService("mail", mail);
            FTP ftp = new FTP();
            AddService("ftp", ftp);
        }

        public void Run()
        {
            TCPListener tcp_listener = new TCPListener();
            AddListener(tcp_listener, "tcp");
            
            /*
            RS232Listener rS232Listener = new RS232Listener();
            AddListener(rS232Listener, "rs");

            UDPListener udp_listener = new UDPListener();
            AddListener(udp_listener, "udp");

            FileListener file_listener = new FileListener();
            AddListener(file_listener, "file");

            NetRemotingListener netRemotingListener = new NetRemotingListener();
            AddListener(netRemotingListener, ".net");
            */
            // odpowiadacz konsolowy
            ConsoleCommunicator cc = new ConsoleCommunicator();
            AddCommunicator(cc);
        }

        string ServiceCenter(string command)
        {
            // serviceId to Identyfikator servicu którego chcemy użyć
            string serviceId = command.Split()[0];
            ICommandAnswerer service;

            // następnie sprawdzamy w naszym zbiorze serwisów czy taka opcja istnieje 
            services.TryGetValue(serviceId, out service);
            if (service == null)
                return "ERROR: unknown service\n";
            
            // Tworzymy informację zwrotną jakiegokolwiek serwisu implementującego ICommandAnswerer
            string ret = service.AnswerCommand(command);
            if (!ret.EndsWith("\n"))
            {
                ret += "\n";
            }
            return ret;
        }
        static void Main(string[] args)
        {
            Serwer serwer = new Serwer();
            serwer.Run();
        }
    }
}
