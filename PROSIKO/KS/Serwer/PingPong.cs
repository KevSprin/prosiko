﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSK
{
    public class PingPong : ICommandAnswerer
    {
        public string AnswerCommand(string command)
        {
            command = command.Replace("ping", "pong");
            return command;
        }
    }
}
