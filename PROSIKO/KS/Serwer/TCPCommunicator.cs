﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace PSK
{
    public class TCPCommunicator : ICommunicator
    {
        private TcpClient client;
        public string Name => "tcp";
        private CommunicatorD onDisconnect;
        public TCPCommunicator(TcpClient client)
        {
            this.client = client;
        }
        public void Start(CommandD onCommand, CommunicatorD onDisconnect)
        {
            this.onDisconnect = onDisconnect;
            // Ta funkcja będzie czytała wysyłaną wiadomość Klienta i będzie ją za pomocą Service Center przetwarzała (ServiceCenter jako delegat onCommand)
            try
            {
                byte[] bytes = new byte[256];
                string data = null;
                int len, nl;
                NetworkStream stream = client.GetStream();
                while ((len = stream.Read(bytes, 0, bytes.Length)) > 0)
                {
                    data += Encoding.ASCII.GetString(bytes, 0, len);
                    while ((nl = data.IndexOf('\n')) != -1)
                    {
                        string line = data.Substring(0, nl + 1);
                        data = data.Substring(nl + 1);
                        // tutaj będzie wiadomość przetwarzana przez ServiceCenter
                        byte[] msg = Encoding.ASCII.GetBytes(onCommand(line));
                        Console.WriteLine($"Otrzymałem wiadomość: {line} , przez TCP");
                        Console.WriteLine($"Zwracam wiadomość: {Encoding.UTF8.GetString(msg)} , przez TCP");
                        // Piszemy do strumienia informację zwrotną z ServiceCenter
                        stream.Write(msg, 0, msg.Length);
                    }
                }
            }
            catch (SocketException)
            {
                Console.WriteLine("TCP Connection has been closed");
            }
            catch (Exception)
            {
            }
        }

        public void Stop()
        {
            client.Close();
            onDisconnect(this);
        }

    }
}
