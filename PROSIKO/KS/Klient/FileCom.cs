﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSK
{
    class FileCom : ICommandAnswerer
    {
        private int FileId;
        private string FileName;
        private string path;
        private string message = null;
        private FileSystemWatcher fileSystemWatcher = new FileSystemWatcher();
        private string in_file = null;
        private string out_file = null;

        Random rng = new Random();

        public FileCom(string path)
        {
            this.path = path;
            fileSystemWatcher.Path = this.path;
            fileSystemWatcher.Created += FileSystemWatcher_Created;
            fileSystemWatcher.Changed += FileSystemWatcher_Changed;
            fileSystemWatcher.Filter = "*.out";
            fileSystemWatcher.EnableRaisingEvents = true;
        }

        public string AnswerCommand(string command)
        {
            FileId = rng.Next(1000, 2000);
            FileName = $"{FileId}.in";
            in_file = $@"{path}\{FileName}";
            FileInfo file_info = new FileInfo($@"{path}\{FileName}");
            DirectoryInfo directory_info = new DirectoryInfo(path);
            if (!directory_info.Exists) return $"Katalog {path} nie istnieje.";
            if (file_info.Exists) return $"Plik {FileName} już istnieje";
            
            var sendBytes = Encoding.ASCII.GetBytes(command);
            File.WriteAllText(file_info.FullName, command);
            while (true)
            {
                if(message != null)
                {
                    string answer = message;
                    message = null;
                    return answer;
                }
            }
        }

        private void FileSystemWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            using (StreamReader sr = new StreamReader(e.FullPath))
            {
                message = sr.ReadToEnd();
                out_file = e.FullPath;
            }
            File.Delete(e.FullPath);
        }
        
        private void FileSystemWatcher_Created(object sender, FileSystemEventArgs e)
        {
            if (Path.GetExtension(e.FullPath) == ".in")
            {
                var out_file_name = Path.GetFileNameWithoutExtension(e.FullPath) + ".out";
                out_file = Path.GetDirectoryName(e.FullPath) + $@"\{out_file_name}";
            }
        }

        public static bool IsFileReady(string filename)
        {
            // If the file can be opened for exclusive access it means that the file
            // is no longer locked by another process.
            try
            {
                using (FileStream inputStream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.None))
                    return inputStream.Length > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
