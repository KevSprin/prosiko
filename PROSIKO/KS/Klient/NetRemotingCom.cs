﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Net.Sockets;

namespace PSK
{
    public class NetRemotingCom : ICommandAnswerer
    {
        private NetRemotingCommunicator remotableObject;

        public NetRemotingCom()
        {
            TcpChannel channel = new TcpChannel();
            ChannelServices.RegisterChannel(channel, false);
            remotableObject = (NetRemotingCommunicator)Activator.GetObject(typeof(NetRemotingCommunicator), "tcp://localhost:8080/Test");
        }

        public string AnswerCommand(string command)
        {
            string odpowiedz = "";
            try
            {
                odpowiedz = remotableObject.GetCall(command);
                
            }
            catch (RemotingException)
            {
                odpowiedz = "Serwer .net remoting niedostępny";
            }
            catch (SocketException)
            {
                odpowiedz = "Serwer .net remoting niedostępny";
            }
            return odpowiedz;
        }
    }
}
