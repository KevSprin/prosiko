﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PSK
{
    class Klient
    {
        static ICommandAnswerer ActiveCommunicator;
        static ICommandAnswerer TCPCommunicator;
        static ICommandAnswerer UDPCommunicator;
        static ICommandAnswerer RSCommunicator;
        static ICommandAnswerer FileCommunicator;
        static ICommandAnswerer NetRemotingCommunicator;
        static string activeProtocol;
        static void Main(string[] args)
        {
            activeProtocol = "TCP";
            Thread t = new Thread(() =>
            {
                TCPCommunicator = new TCPCom("localhost", 12345);
                ActiveCommunicator = TCPCommunicator;
            });
            t.Start();
            
            /*
            UDPCommunicator = new UDPCom("localhost", 12346);
            RSCommunicator = new RS232Com();
            FileCommunicator = new FileCom(Directory.GetCurrentDirectory() + @"\..\..\..\Pliki");
            NetRemotingCommunicator = new NetRemotingCom();
            */
            ConsoleCommunicator cc = new ConsoleCommunicator();
            Console.WriteLine("W razie problemów używania konsoli wpisz <pomoc> aby dowiedzieć się o dozwolonych kommendach.");
            
            while (true)
            {
                Console.Write($"({activeProtocol}) >> ");
                string command = Console.ReadLine();
                switch (command.Split()[0])
                {
                    case "testujping":
                        int liczba_iteracji = Convert.ToInt32(command.Split()[1]);
                        int rozmiar_polecenia = Convert.ToInt32(command.Split()[2]);
                        TestujPing(liczba_iteracji, rozmiar_polecenia);           
                        break;
                    case "testujcfg":
                        UsługaCfg(command);
                        break;
                    case "testujmail":
                        WyślijWiadomość(command);
                        break;
                    case "zmienprotokol":
                        CommunicatorChoice(command);
                        break;
                    case "testujftp":
                        TestujFTP(command);
                        break;
                    case "pomoc":
                        Help();
                        break;
                }
            }
        }
        
        private static void UsługaCfg(string command)
        {

            command = command.Replace("testujcfg", "cfg");
            command += "\n";
            Console.WriteLine(ActiveCommunicator.AnswerCommand(command));
        }

        private static void TestujPing(int liczba_iteracji, int rozmiar_polecenia)
        {
            string polecenie = $"ping {rozmiar_polecenia} ";
            for(int i = polecenie.Length-1; i < rozmiar_polecenia-2; ++i)
            {
                polecenie += "0";
            }
            polecenie += "\n";
            Stopwatch stopwatch = Stopwatch.StartNew();
            int byte_count_after = 0, byte_count_before = liczba_iteracji * rozmiar_polecenia;
            for (int i = 0; i < liczba_iteracji; i++)
            {
                try
                {
                    string answer = ActiveCommunicator.AnswerCommand(polecenie);
                    //Console.WriteLine(answer.Length);
                    byte_count_after += answer.Length;
                    Console.WriteLine(answer);
                }
                catch (ArgumentNullException)
                {
                    Console.WriteLine("Wysyłanie ping'a zakończyło się niepowodzeniem.");
                    return;
                }
            }
            stopwatch.Stop();
            Console.WriteLine($"Wysłałem ciąg bajtów o wielkości {byte_count_before} i otrzymałem {byte_count_after} w ciągu {stopwatch.ElapsedMilliseconds} milisekund");            
        }

        private static void WyślijWiadomość(string command)
        {
            string mode = "";
            string user = "";
            try
            {
                mode = command.Split()[1];
                user = command.Split()[2];
            }
            catch
            {
                Console.WriteLine("Missing parameters!");
                return;
            }
            switch (mode)
            {
                case "get":
                    command = $"mail {mode} {user}";
                    break;
                case "send":
                    string sendTo = command.Split()[3];
                    string message = command.Substring(command.IndexOf(sendTo) + sendTo.Length + 1);
                    command = $"mail {mode} {user} {sendTo} {message}";
                    break;
                default:
                    Console.WriteLine("Unknown command (user either get or send)");
                    return;
            }
            command += "\n";
            Console.WriteLine(ActiveCommunicator.AnswerCommand(command));
        }

        private static void TestujFTP(string command)
        {
            string mode;
            string SciezkaPlikowKlienta = Directory.GetCurrentDirectory() + @"\..\..\PlikiKlient";
            try
            {
                mode = command.Split()[1];
            }
            catch
            {
                Console.WriteLine("Nie podano argumentu. [send | get]");
                return;
            }
            string nazwa_pliku = command.Split()[2];
            string sciezka = $@"{SciezkaPlikowKlienta}\{nazwa_pliku}";
            try
            {
                switch (mode)
                {
                    case "get":
                        command = $"ftp {mode} {nazwa_pliku}";
                        command += "\n";
                        string plik_base64 = ActiveCommunicator.AnswerCommand(command);
                        byte[] dane_pliku = Convert.FromBase64String(plik_base64);
                        using (var fs = new FileStream(sciezka, FileMode.Create, FileAccess.Write))
                        {
                            fs.Write(dane_pliku, 0, dane_pliku.Length);
                        }
                        break;
                    case "send":
                        dane_pliku = File.ReadAllBytes(sciezka);
                        plik_base64 = Convert.ToBase64String(dane_pliku);
                        command = $"ftp {mode} {nazwa_pliku} {plik_base64}";
                        command += "\n";
                        Console.WriteLine(ActiveCommunicator.AnswerCommand(command));
                        break;
                    default:
                        Console.WriteLine("Niewłaściwy argument.");
                        return;
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Operacja z FTP zakończyła się niepowodzeniem!");
            }
        }

        private static void CommunicatorChoice(string command)
        {
            string choice;
            try
            {
                choice = command.Split()[1];
            }
            catch
            {
                Console.WriteLine("Nie podano argumentu. Wybierz jeden z istniejących protokołów.");
                return;
            }
            switch (choice)
            {
                case "tcp":
                    ActiveCommunicator = TCPCommunicator;
                    activeProtocol = "TCP";
                    break;
                case "udp":
                    ActiveCommunicator = UDPCommunicator;
                    activeProtocol = "UDP";
                    break;
                case "file":
                    ActiveCommunicator = FileCommunicator;
                    activeProtocol = "FILE";
                    break;
                case ".net":
                    ActiveCommunicator = NetRemotingCommunicator;
                    activeProtocol = ".net Remoting";
                    break;
                case "rs":
                    ActiveCommunicator = RSCommunicator;
                    activeProtocol = "RS-232";
                    break;
                default:
                    Console.WriteLine("Niewłaściwy wybór protokołu");
                    break;
            }
        }

        private static void Help()
        {
            Console.WriteLine("Można wybierać spośród następujących kommend:");
            Console.WriteLine("\ttestujping <liczba żądań> <wielkość kommunikatu>");
            Console.WriteLine("\ttestujmail get <nazwa uzytkownika>");
            Console.WriteLine("\ttestujmail send <nadawca> <odbiorca> <wiadomosc>");
            Console.WriteLine("\ttestujftp send <nazwa_pliku>");
            Console.WriteLine("\ttestujftp get <nazwa_pliku>");
            Console.WriteLine("\ttestujcfg [ media | uslugi ]");
            Console.WriteLine("\ttestujcfg [ stop | start ] [ tcp | udp | file | .net | rs ]");
            Console.WriteLine("\tzmienprotokol [ tcp | udp | file | .net | rs ]");
        }
    }
}
