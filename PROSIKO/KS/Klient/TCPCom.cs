﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace PSK
{
    public class TCPCom : ICommandAnswerer
    {
        private TcpClient client;
        private string serverAddress;
        private int port;
        public TCPCom(string serverAddress, int port)
        {
            this.serverAddress = serverAddress;
            this.port = port;
            client = new TcpClient(serverAddress, port);
        }
        public string AnswerCommand(string command)
        {          
            try
            {
                if (!client.Connected)
                {
                    client = new TcpClient(serverAddress, port);
                    Console.WriteLine("Połączono z serwerem (TCP)");
                }
                byte[] bytes = new byte[256];
                string data = null;
                int len, nl;
                byte[] cmdBytes = Encoding.ASCII.GetBytes(command);
                NetworkStream stream = client.GetStream();
                stream.Write(cmdBytes, 0, cmdBytes.Length);
                while ((len = stream.Read(bytes, 0, bytes.Length)) > 0)
                {
                    data += Encoding.ASCII.GetString(bytes, 0, len);
                    if ((nl = data.LastIndexOf('\n')) != -1)
                    {
                        return data.Substring(0, nl + 1);
                    }
                }
            }
            catch (SocketException)
            {
                Console.WriteLine("Serwer TCP niedostępny!");
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            client.Close();
            return "Połączenie TCP przerwane";
        }
    }
}
