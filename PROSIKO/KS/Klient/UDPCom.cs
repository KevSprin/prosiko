﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace PSK
{
    public class UDPCom : ICommandAnswerer
    {
        private UdpClient client;
        private string serverAddress;
        private int port;
        private int max_porcja = 1500;
        public UDPCom(string serverAddress, int port)
        {
            client = new UdpClient();
            this.serverAddress = serverAddress;
            this.port = port;
            client.Connect(serverAddress, port);
            client.Client.ReceiveTimeout = 1000;
            client.Client.SendTimeout = 1000;
            client.DontFragment = false;

        }

        public string AnswerCommand(string command)
        {
            string result = "";
            try
            {
                var sendBytes = Encoding.ASCII.GetBytes(command);
                if(sendBytes.Length > max_porcja)
                {
                    var porcje = PorcjonowanieDanych(sendBytes);
                    foreach (var porcja in porcje)
                    {
                        client.Send(porcja, porcja.Length);
                    }
                }
                else
                    client.Send(sendBytes, sendBytes.Length);

                IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
                var receiveBytes = client.Receive(ref RemoteIpEndPoint);
                result = Encoding.ASCII.GetString(receiveBytes);
                while (receiveBytes.Length == max_porcja)
                {
                    receiveBytes = client.Receive(ref RemoteIpEndPoint);
                    result += Encoding.ASCII.GetString(receiveBytes);
                }         
            }
            catch (SocketException)
            {
                Console.WriteLine("Serwer UDP niedostępny");
                result = "Połączenie UDP przerwane";
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                client.Close();
                result = e.ToString();
            }
            return result;
        }

        private IList<byte[]> PorcjonowanieDanych(byte[] dane)
        {
            IList<byte[]> porcje = new List<byte[]>();
            int start = 0;
            int mnoznik = dane.Length / max_porcja;
            int reszta = dane.Length - max_porcja * mnoznik;

            for (int i = 0; i < mnoznik; ++i)
            {
                byte[] tmp = new byte[max_porcja];
                Array.Copy(dane, start, tmp, 0, max_porcja);
                porcje.Add(tmp);
                start += max_porcja;
            }
            byte[] reszta_danych = new byte[reszta];
            Array.Copy(dane, start, reszta_danych, 0, reszta);
            porcje.Add(reszta_danych);
            return porcje;
        }
    }
}
