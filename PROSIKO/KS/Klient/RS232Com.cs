﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;

namespace PSK
{
    public class RS232Com : ICommandAnswerer
    {
        private SerialPort client;
        private string message = null;
        private bool IsConnected = true;
        public RS232Com()
        {
            client = new SerialPort();
            client.PortName = "COM2";
            client.BaudRate = 57600;
            client.Parity = Parity.None;
            client.DataBits = 8;
            client.StopBits = StopBits.One;
            client.Handshake = Handshake.None;
            client.RtsEnable = true;
            client.WriteTimeout = 500;
            client.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
            client.Open();
        }


        public string AnswerCommand(string command)
        {
            if (!IsConnected) return "Serwer RS-232 niedostępny!";
            client.WriteLine(command);
            while(true)
            {
                if(message != null)
                {
                    string answer = message;
                    message = null;
                    return answer;
                }
            }
        }

        private void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            message = sp.ReadExisting();
            if (message == "Closed") {
                IsConnected = false;
            }
            else if (message == "Open")
            {
                IsConnected = true;
                message = null;
            }
        }
    }
}
