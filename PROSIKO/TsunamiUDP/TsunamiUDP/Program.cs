﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TsunamiUDP
{
    class Program
    {
        static TcpClient tcpClient;
        static UdpClient udpClient;
        static string path = Directory.GetCurrentDirectory() + @"\..\..\PlikiKlienta";

        static void Help()
        {
            Console.WriteLine("get <nazwa pliku>");
        }

        static void GetRequest(string command)
        {
            string nazwa_pliku = command.Split()[1];
            List<byte[]> dane = new List<byte[]>();
            List<int> partsizes = new List<int>();
            List<int> fail_packet_index = new List<int>();
            byte[] nazwa_pliku_bajty = Encoding.ASCII.GetBytes(command);
            NetworkStream stream = tcpClient.GetStream();
            
            // wysłanie żądania do serwera o plik
            stream.Write(nazwa_pliku_bajty, 0, nazwa_pliku_bajty.Length);
            byte[] odp = new byte[256];

            // odbieranie informacji zwrotnej z serwera wielkość pliku oraz ilość porcji
            stream.Read(odp, 0, odp.Length);
            string odp_string = Encoding.ASCII.GetString(odp).Replace("\0", string.Empty);
            Console.WriteLine(odp_string);
            int tasks = Convert.ToInt32(odp_string.Split()[1]);
            for(int i = 0; i < tasks; ++i)
            {
                // żądanie o pakiet
                string msg = $"get {i}";
                byte[] msg_bytes = Encoding.ASCII.GetBytes(msg);
                stream.Write(msg_bytes, 0, msg_bytes.Length);

                // informacja o pakiecie
                byte[] msg_answer_bytes = new byte[256];
                stream.Read(msg_answer_bytes, 0, msg_answer_bytes.Length);
                string msg_answer = Encoding.ASCII.GetString(msg_answer_bytes).Replace("\0", string.Empty);
                Console.WriteLine(msg_answer);

                // zbieranie informacji o wielkosci pakietu
                int partsize = Convert.ToInt32(msg_answer);
                partsizes.Add(partsize);

                // odbieranie pliku
                IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
                var receiveBytes = udpClient.Receive(ref remoteEndPoint);
                if (receiveBytes.Length != partsize)
                {
                    fail_packet_index.Add(i);
                }
                dane.Add(receiveBytes);
            }
            
            // code redundancy -> cleanup later
            foreach (var index in fail_packet_index)
            {
                // żądanie o pakiet
                string msg = $"get {index}";
                byte[] msg_bytes = Encoding.ASCII.GetBytes(msg);
                stream.Write(msg_bytes, 0, msg_bytes.Length);

                // informacja o pakiecie
                byte[] msg_answer_bytes = new byte[256];
                stream.Read(msg_answer_bytes, 0, msg_answer_bytes.Length);
                string msg_answer = Encoding.ASCII.GetString(msg_answer_bytes).Replace("\0", string.Empty);
                Console.WriteLine(msg_answer);

                // zbieranie informacji o wielkosci pakietu
                int partsize = Convert.ToInt32(msg_answer);
                partsizes.Add(partsize);

                // odbieranie pliku
                IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
                var receiveBytes = udpClient.Receive(ref remoteEndPoint);
                dane[index] = receiveBytes;
            }

            string end = "END";
            byte[] end_byte = Encoding.ASCII.GetBytes(end);
            stream.Write(end_byte, 0, end_byte.Length);

            // scalanie byte arrayów i zapis do pliku
            byte[] result = new byte[0];           
            for(int i = 0; i < dane.Count; ++i)
            {
                var dane_array = dane[i];
                result = result.Concat(dane_array).ToArray();
            }
            using (var fs = new FileStream($@"{path}\{nazwa_pliku}", FileMode.Create, FileAccess.Write))
            {
                fs.Write(result, 0, result.Length);
            }
        }

        static void Main(string[] args)
        {
            tcpClient = new TcpClient("localhost", 12345);
            udpClient = new UdpClient(12346);
            //udpClient.Connect("localhost", 12346);
            udpClient.DontFragment = false;

            Console.WriteLine(">>> TSUNAMI UDP <<<");
            Console.WriteLine("Wpisz <pomoc> by zobaczyć jak skorzystać:");
            while (true)
            {
                Console.Write(">> ");
                string command = Console.ReadLine().ToString();
                string prefix = command.Split()[0];
                switch (prefix)
                {
                    case "pomoc":
                        Help();
                        break;
                    case "get":
                        GetRequest(command);
                        break;
                    default:
                        break;
                }
                
            }
        }
    }
}
