﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TsunamiUDPSerwer
{
    public class Program
    {
        static TcpListener tcpListener;
        static TcpClient client;
        static UdpClient udpClient;
        static int max_porcja = 1500;
        static string path = Directory.GetCurrentDirectory() + @"\..\..\PlikiSerwera";

        static IList<byte[]> PorcjonowanieDanych(byte[] dane)
        {
            IList<byte[]> porcje = new List<byte[]>();
            int start = 0;
            int mnoznik = dane.Length / max_porcja;
            int reszta = dane.Length - max_porcja * mnoznik;

            for (int i = 0; i < mnoznik; ++i)
            {
                byte[] tmp = new byte[max_porcja];
                Array.Copy(dane, start, tmp, 0, max_porcja);
                porcje.Add(tmp);
                start += max_porcja;
            }
            byte[] reszta_danych = new byte[reszta];
            Array.Copy(dane, start, reszta_danych, 0, reszta);
            porcje.Add(reszta_danych);
            return porcje;
        }


        static void Send()
        {
            byte[] bytes = new byte[256];
            NetworkStream stream = client.GetStream();
            stream.Read(bytes, 0, bytes.Length);
            string command = Encoding.ASCII.GetString(bytes);
            Console.WriteLine(command);
            string nazwa_pliku = command.Split()[1].Replace("\0",string.Empty);
            string sciezka = $@"{path}\{nazwa_pliku}";
            byte[] file = File.ReadAllBytes(sciezka);
            int filesize = file.Length;
            int ilosc_taskow = 1;
            IList<byte[]> porcje =new List<byte[]>();

            if (filesize > max_porcja)
            {
                porcje = PorcjonowanieDanych(file);
                ilosc_taskow = porcje.Count();
            }
            else
                porcje.Add(file);

            // wysłanie informacje o wielkości plików i ilości 
            string msg = $"{filesize} {ilosc_taskow}";
            byte[] byte_msg = Encoding.ASCII.GetBytes(msg);
            stream.Write(byte_msg, 0, byte_msg.Length);

            for (int i = 0; i < ilosc_taskow; ++i)
            {
                // czekanie na żądanie od klienta                
                byte[] request = new byte[256];
                stream.Read(request, 0, request.Length);
                string request_string = Encoding.ASCII.GetString(request);
                Console.WriteLine(request_string);
                int taskid = Convert.ToInt32(request_string.Split()[1]);

                // wysłanie informacji o żądanym pakiecie
                string packet_info = $"{porcje[taskid].Length}";
                byte[] packet_info_bytes = Encoding.ASCII.GetBytes(packet_info);
                stream.Write(packet_info_bytes, 0, packet_info_bytes.Length);

                // wysłanie porcji
                udpClient.Send(porcje[taskid], porcje[taskid].Length);
            }

            // odbieranie informacji o zagubionych lub uszkodzonych pakietach
            byte[] feedback_message_bytes = new byte[256];
            stream.Read(feedback_message_bytes, 0, feedback_message_bytes.Length);
            string feedback_message = Encoding.ASCII.GetString(feedback_message_bytes);
            string prefix = feedback_message.Split()[0].Replace("\0", string.Empty);
            if(prefix != "END")
            {
                var indexy = feedback_message.Split().Select(x => Convert.ToInt32(x)).ToArray();
                foreach (var index in indexy)
                {
                    // czekanie na żądanie od klienta                
                    byte[] request = new byte[256];
                    stream.Read(request, 0, request.Length);
                    string request_string = Encoding.ASCII.GetString(request);
                    Console.WriteLine(request_string);
                    int taskid = Convert.ToInt32(request_string.Split()[1]);

                    // wysłanie informacji o żądanym pakiecie
                    string packet_info = $"{porcje[taskid].Length}";
                    byte[] packet_info_bytes = Encoding.ASCII.GetBytes(packet_info);
                    stream.Write(packet_info_bytes, 0, packet_info_bytes.Length);

                    // wysłanie porcji
                    udpClient.Send(porcje[taskid], porcje[taskid].Length);
                }
            }

        }

        static void Main(string[] args)
        {
            tcpListener = new TcpListener(IPAddress.Any, 12345);
            tcpListener.Start();
            udpClient = new UdpClient();
            udpClient.Connect("localhost", 12346);

            while (true)
            {
                client = tcpListener.AcceptTcpClient();
                if(client != null)
                {
                    Send();
                }

            }
        }
    }
}
